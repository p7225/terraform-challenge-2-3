### providers definitions
terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.8.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.12.0"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_auth_token
}

provider "google-beta" {
  credentials = file(var.gcp_auth_file_path)
}

provider "google" {
  credentials = file(var.gcp_auth_file_path)
}
####

# create kubernetes cluster on gcp 
module "k8s-cluster" {
  source          = "./k8s-gcp-cluster"  
  project_id      = var.gcp_project_id
}


# create service account for gitlab access and binds to cluster-admin role
module "k8s-git-service-account" {
  source       = "./k8s-git-service-account"
  gke_cluster_ca_certificate = module.k8s-cluster.ca_certificate
  gke_auth_host              = module.k8s-cluster.host
  gke_auth_token             = module.k8s-cluster.token
}

# bind and integrate k8s cluster with gitlab
module "git-group-cluster" {
  source        = "./gitlab-group-cluster"
  depends_on    = [module.k8s-git-service-account]

  gitlab_auth_token           = var.gitlab_auth_token
  gitlab_group_full_path      = var.gitlab_group_full_path
  group_management_project_id = var.group_management_project_id

  group_kubernetes_api_url = module.k8s-cluster.host
  group_kubernetes_token   = module.k8s-git-service-account.service_account_token
  group_kubernetes_ca_cert = module.k8s-git-service-account.service_account_ca_certificate

}