# this module creates service account, binds it to cluster-admin
# and as a result returns token for service accoiunt that is later passed to gitlab for integration

terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.8.0"
    }
  }
}

# provider is initialized with admin credentials from kubernetes cluster that was previously created
provider "kubernetes" {
  cluster_ca_certificate = var.gke_cluster_ca_certificate
  host                   = var.gke_auth_host
  token                  = var.gke_auth_token
}
# service account named "gitlab"
resource "kubernetes_service_account" "gitlab" {
  metadata {
    name = "gitlab"
  }
}
# read default secret from gitlab account
# result is token and ca certificate 
data "kubernetes_secret" "gitlab-secret" {
  metadata {
    name = kubernetes_service_account.gitlab.default_secret_name
  }
}

#binding of service account to cluster role
resource "kubernetes_role_binding" "gitlab-service-cluster-binding" {
  metadata {
    name      = "gitlab-service-account-role-binding"
    namespace = "default"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab"
    namespace = "default"
  }
    
}