output "service_account_token" {
  description = "Token"
  value       = data.kubernetes_secret.gitlab-secret.data["token"]
}

output "service_account_ca_certificate"{
  description = "ca certificate"
  value       = data.kubernetes_secret.gitlab-secret.data["ca.crt"]
}