variable "gitlab_auth_token" {
}
variable "gitlab_group_full_path" {
}
variable "group_management_project_id" {
}
variable "group_kubernetes_api_url" {
}
variable "group_kubernetes_token" {
}
variable "group_kubernetes_ca_cert" {
}
variable "group_cluster_name" {
  default     = "parbol-cluster-challenge"
}


