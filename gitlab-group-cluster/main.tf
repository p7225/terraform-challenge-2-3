terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.12.0"
    }
  }
}
### On GitLab SaaS, you must use the GitLab UI to create groups without a parent group. You cannot use the API to do this.
### https://docs.gitlab.com/ee/api/groups.html#new-group
# resource "gitlab_group" "parbol-challenge" {
#   name = "parbol-challenge"
#   path = "https://gitlab.com/parbol-challenge"
# }

## find existing parent group by path
data "gitlab_group" "parbolgroup" {
  full_path = var.gitlab_group_full_path
}

resource "gitlab_group_cluster" "parbol-challenge-cluster" {
  group                         = data.gitlab_group.parbolgroup.group_id
  name                          = var.group_cluster_name
  domain                        = "gitlab.com"
  enabled                       = true
  kubernetes_api_url            = var.group_kubernetes_api_url
  kubernetes_token              = var.group_kubernetes_token
  kubernetes_ca_cert            = var.group_kubernetes_ca_cert
  kubernetes_authorization_type = "rbac"
  environment_scope             = "*"
  management_project_id         = var.group_management_project_id
}



