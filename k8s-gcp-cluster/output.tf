output "cluster_name" {
  description = "Cluster name"
  value       = module.gke.name
}

output "ca_certificate" {
  description = "Kubernetes certificate"
  value       = module.gke_auth.cluster_ca_certificate
}
output "host" {
  description = "Kubernetes host endpoinf for API"
  value       = module.gke_auth.host
}
output "token" {
  description = "Kubernetes token"
  value       = module.gke_auth.token
}