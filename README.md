# Parbol terraform challenge 2-3

For challenges 2 and 3 tasks are connected in following steps:
 - install Kubernetes cluster to simulate existing production
 - create service account on kubernetes cluster for gitlab access, bind it to cluster-admin role
 - create gitlab group cluster and bind kubernetes gitlab account with Gitlab group. That will end with Kubernetes cluster integration with Gitlab group
 - for all steps use terraform where possible

# How to test code

## Prerequisites

Install terraform on machine or use docker with terraform.
Open account on GCP, prepare authentication JSON file for GCP access. Enable all necessary API access on GCP.
Change github authentication token, path to GCP authentication file, GCP project, Gitlab group name and ID of management project (project with Kubernetes deployments) in `terraform.tfvars` 

## What will terraform code do?

First it will create dummy kubernetes cluster on GCP.
Admin credentials from new kubernetes cluster will be used to create service account `gitlab` and that account will be connected to `cluster-admin` role with ClusterRoleBinding.
Authentication token from new serviceaccount `gitlab` will be used to connect Gitlab group with Kubernetes, ending with kubernetes integration in Gitlab 

## Run terraform
In current folder execute
```sh
terraform init
```

To check what will be done  
```sh 
terraform plan
```

To apply all changes 
```sh
terraform apply
```

To destroy all assets
```sh 
terraform destroy
```